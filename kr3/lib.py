from matplotlib import pyplot
from numpy import poly1d as np_poly1d, polyfit as np_polyfit
from copy import deepcopy


def matrix(amatrix, bmatrix):
    bmatrix = list(zip(*bmatrix))
    return [
        [
            sum(x * y for x, y in zip(amatrix_row, bmatrix_column))
            for bmatrix_column in bmatrix
        ]
        for amatrix_row in amatrix
    ]


def trendgraph(dimensions, matrixtime):
    pyplot.plot(dimensions, matrixtime, "-o")  # Точки времени вычисления
    trend = np_poly1d(np_polyfit(dimensions, matrixtime, 1))   # Проводим линию тренда
    pyplot.plot(dimensions, trend(dimensions), "b", c="red", label=f"Тренд вычислений")

    pyplot.xlabel("Размерность матриц")
    pyplot.ylabel("Время вычисления в секундах: ")

    pyplot.show()

def gauss(rows_count, matrix):
    # Прямой ход метода Гаусса
    # Смысл метода: Последовательно исключаем переменную за переменной, пока в одной из строк не будет однозначно определена переменная xi.
    for i in range(rows_count):
        for j in range(rows_count):
            if i != j:
                ratio = matrix[j][i] / matrix[i][i]
                for k in range(rows_count+1):
                    # Заменяем
                    matrix[j][k] = matrix[j][k] - ratio * matrix[i][k]


    result_vector = []
    # Обратный ход метода Гаусса.
    for i in range(rows_count):
        vector_coord = round(matrix[i][rows_count] / matrix[i][i], 2)
        result_vector.append(vector_coord)

    return result_vector


def calculation(rows_count, matrix):
    # Если количество строк в матрице = 1, определитель равен первой ячейке матрицы
    if rows_count == 1:
        return matrix[0][0]
    # Если же количество строк в матрице = 2, то определитель вычисляется по формуле.
    elif rows_count == 2:
        return matrix[0][0] * matrix[1][1] - matrix[1][0] * matrix[0][1]
    # Иначе определитель высчитывается рекурсивно через определители более маленьких матриц
    else:
        determinant = 0
        for i in range(rows_count):
            new_matrix = deepcopy(matrix)
            new_matrix.pop(0)
            for j in range(rows_count - 1):
                new_matrix[j].pop(i)
            determinant += (
                    (-1) ** i * matrix[0][i] * calculation(rows_count=rows_count - 1, matrix=new_matrix)
            )
        return determinant # Выводим определитель


def cramer(rows_count, matrix):
    # Находим определитель matrix_determinant исходной матрицы marix.
    matrix_determinant = calculation(rows_count, matrix)
    if matrix_determinant == 0:  # Нельзя решить
        raise ValueError

    result_vector = []
    for i in range(rows_count):# Считаем определители
        line = deepcopy(matrix)
        for j in range(rows_count):  # Заменяем значения в матрице на свободные коэффициенты.
            line[j][i] = matrix[j][-1]
        # Вычисляем определитель матрицы со свободными коэффициентами и делим на определитель матрицы.
        line_determinant = round(calculation(rows_count=rows_count, matrix=line) / matrix_determinant, 2)
        result_vector.append(line_determinant)

    return result_vector # Выводим результат

def saxpy(x, y, a):   # Алгоритм SAXPY
    saxpyvector = [
        x * a + y  # Считаем по формуле
        for x, y in zip(x, y)  # Совместный проход
    ]
    return saxpyvector

