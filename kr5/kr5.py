from lib import gauss, cramer

rows_count = int(input('Количество уравнений в системе:\t'))

# Матрица будет включать в себя и систему уравнений, и свободные коэффициенты на конце каждой строки матрицы.
matrix = []
for row in range(rows_count):
    matrix_row = []
    for col in range(rows_count + 1):
        matrix_row.append(float(input(f"{col} Введите значение матрицы:\t")))
    matrix.append(matrix_row.copy())

choice = input("Gauss-Jordan (1) или Cramer (2):")
if choice == "1":
    result_vector = gauss(rows_count, matrix)
elif choice == "2":
    result_vector = cramer(rows_count, matrix)
for i in range(rows_count):
    print(f"X{i} = {result_vector[i]}", end='\t')


