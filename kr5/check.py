import time
import random
from lib import gauss, cramer
from sys import getsizeof

time1 = []  # Массив старта времени
time2 = []  # Массив конца времени
n_lst = []
matrixsize = []

for rows_count in range(10, 110, 10):
    matrix = []
    for row in range(rows_count):
        time_temp = 0
        matrix_row = [] # Строки матрицы
        for col in range(rows_count + 1):
            matrix_row.append(random.randint(1, 11)) # Заполняем матрицу случайными числами
        matrix.append(matrix_row.copy())
    matrix_size = getsizeof(matrix)
    print(f" Объем занимаемой оперативной памяти (б): {matrix_size} ")
    file = open("matrixsize.txt", 'a')  # Создаем файл, разрешена дозапись в файл
    matrixsize.append(matrix_size)
    file.write(f'{matrixsize[-1]}\n')

    for _ in range(3):
        start = time.time()  # Начало функции
        result_vector = gauss(rows_count, matrix)  # Вызываем функцию gauss (Проводим вычисления)
        end = time.time()    # Начало функции
        time_temp += end - start  # Время, которое заняло выполнение функции

    time1.append(time_temp / 3)
    file = open("timegauss.txt", 'a')  # Создаем файл, разрешена дозапись в файл
    file.write(f'{time1[-1]:.8f}\n')  # Записываем время в файл
    print(f'Среднее значение времени Гаусса: {time1[-1]}')
    n_lst.append(row)

    time_temp = 0
    for _ in range(3):
        start = time.time()  # Начало функции
        r, m = rows_count, matrix.copy()
        result_vector = cramer(rows_count, matrix)
        end = time.time()  # Начало функции
        time_temp += end - start

    time2.append(time_temp / 3)
    file = open("timecramer.txt", 'a')  # Создаем файл, разрешена дозапись в файл
    file.write(f'{time2[-1]:.8f}\n')  # Записываем время в файл
    print(f'Среднее значение Крамера: {time2[-1]}')
